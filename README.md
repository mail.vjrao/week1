# Build a blockchain - week1 - Data Model

  Model objects in src/models.pyå
      State - a set of accounts (each with its own balance)
      AccountState - state of an individual account (balance )
      Block - a raw block eg "coinbase" in an input file
      Transaction - a raw transaction line eg. "transfer" in input file
      BlockTransaction - A combination of a parsed Block, with parsed Transactions. This will be the contents of a block

# Notes -

    - Because of Decimal usage, simple_json is needed to export/serialize account balances
    Uses : Python version 3.10
    Might need to upgrade to 3.11 to enable some features of Pydantic (syntax like 'str | None' ) - if needed



## Development instructions

```
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements-dev.txt
pre-commit install # to setup git hook scripts
python3 -m pytest
```

## How to run

- Run program
  ```console
  python src/main.py -h
  ```


## Test
```console
python -m pytest     # regular run
python -m pytest -v  # run verbose
python -m pytest --cov-src tests # run tests with coverage report

```


### Filters

- **Skip** a particular test by decorating the test with `@pytest.mark.skip(reason="...")` and then run `python -m pytest [-v]`
  ```python
  # mytest.py

  @pytest.mark.skip(reason="Not implemented yet")
  def test_booboo:
    assert True
  ```

  ```console
  $ pytest -m pytest [-v]

  ```


- **Only** Run one test by decorating the test with `@pytest.mark.only` and then run `python -m pytest -[v]k only`
  ```python
  @pytest.mark.only
  def test_booboo:
    assert True
  ```

  ```console
  $ pytest -m pytest -k only
  ```
