from decimal import Decimal
from functools import cmp_to_key
from typing import List
from models import ParsedTransaction, ParsedBlock, Block, Transaction



class InputParser:

    # custom sort for txns
    @staticmethod
    def sort_fn (tx1:ParsedTransaction, tx2:ParsedTransaction) -> int:
        if tx1.fee > tx2.fee:
            return 1
        else:
            return -1    

    @staticmethod
    def parse_block(line:str, seq:int) -> ParsedBlock:
        _, address = line.split("|")
        return ParsedBlock(address=address.strip(), block_number=seq, raw_line=line.strip())

    @staticmethod
    def parse_transaction(line:str) -> ParsedTransaction:
        row_type, from_address, to_address, amount, fee = line.split("|")
        return ParsedTransaction(
                            from_address=from_address.strip(), 
                            to_address=to_address.strip(), 
                            amount=Decimal(amount), 
                            fee=Decimal(fee),
                            raw_line=line.strip())



    @staticmethod
    def process_input(lines: List[str], verbosity:bool, quiet:bool) -> tuple[bool, List[Block]] :
        ok:bool = True
        block_sequence:int = 0
        block_txns: List[Block] = []
        transactions: List[ParsedTransaction] = []
        prev_hash = "".join( ['0']*64)
        for line in lines:
            if verbosity and not quiet:
                print(line)
            
            if line.startswith("coinbase"):
                block_sequence += 1
                block = InputParser.parse_block(line, block_sequence)
                # using a custom sort key , to sort transactions by fee
                transactions.sort(key=cmp_to_key(InputParser.sort_fn), reverse=True)
                block_txn = Block(block=block, transactions=transactions)
                block_txn.prev_block_hash = prev_hash
                # compute current hash , and save to prev hash
                prev_hash = block_txn.compute_hash()
                block_txns.append(block_txn)
                transactions = []
            elif line.startswith("transfer"):
                raw_transaction = InputParser.parse_transaction(line)
                transactions.append(Transaction(raw_transaction))
            else:
                if not quiet:
                    print(f"❌ Unknown row type: {line}")
                ok = False
                break

        return ok, block_txns