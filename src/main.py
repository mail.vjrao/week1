"""Something something

Usage:
    verify (-d PATH) (-i FILE) [-qv]

Options:
    -d, --dir DIR       Path to the blockchain
    -i, --input FILE    Input file name [default: input.txt]
    -v, --verbose       Verbose output [default: False]
    -q, --quiet         Quiet output [default: False]

"""

import os
import sys
import simplejson  as json

from docopt import docopt
from functools import cmp_to_key
from decimal import Decimal
from typing import List, Tuple, Dict, Any
from models import ParsedTransaction, ParsedBlock, BlockTransaction, State, AccountState
from input_parser import InputParser
from miner import Miner



def main(folder: str, input_file:str, verbosity=False, quiet=False) -> int:
    """Main function
    @param folder: The folder to verify the blockchain in.
    @param input_file: The input file to verify the blockchain in.
    @param verbosity: Whether to print verbose output.
    @param quiet: Whether to print quiet output.
    @return: An integer indicating whether the command is successful
             0 for success, 1 for failure. This makes it easy to compose
             this command in a bash script.

    """

    if verbosity and not quiet:
        print(f"🔎 {folder}")

    if not input_file:
        input_file = "input.txt"
        if verbosity and not quiet:
            print(f"📄 input_file is = {folder}/{input_file}")

    fl = open(f"{folder}/{input_file}", "r")
    lines = fl.readlines()
    fl.close()


    ok, block_txns = InputParser.process_input(lines, verbosity, quiet)

    if ok:
        result, state = Miner.mine_blocks(block_txns)
        if result:
            if verbosity and not quiet:
                if state.is_valid():
                    print(f"✅ Blockchain is valid")
                else:
                    print(f"❌ Blockchain is invalid")
        else:
            if not quiet:
                print(f"❌ Blockchain is invalid")


    # export processed blocks
    if state:
        state.export(folder)
        for block in block_txns:
            block.export(folder)
    return 0 if ok else 1


if __name__ == "__main__":
    arguments = docopt(__doc__, version="something 0.1")
    folder = os.path.join(os.getcwd(), arguments["--dir"])
    input_file_name = arguments["--input"]
    v, q = arguments["--verbose"], arguments["--quiet"]
    sys.exit(main(folder, input_file_name, v, q))
