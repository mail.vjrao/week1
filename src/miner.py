from decimal import Decimal
from functools import cmp_to_key
from typing import List
from models import ParsedTransaction, ParsedBlock, Block, State, AccountState


block_creation_reward:Decimal = Decimal(5)


class Miner:
    @staticmethod
    def mine_blocks(block_txns: List [Block]) -> \
                    tuple[bool, State]:
        """Do something
        @param block_txns: The list of blocks
        @return: A boolean indicating whether the process is successful
        @return: A State object
        """

        state = State(accounts={})

        for block_txn in block_txns:
            block_address = block_txn.block_address
            # give credit for mining a block
            if block_address in state.accounts:
                state.accounts[block_address].balance += block_creation_reward
            else:
                state.accounts[block_address] = AccountState(balance=block_creation_reward)

            # apply transactions
            for txn in block_txn.block_transactions:
                # miner gets the fee
                state.accounts[block_address].balance += txn.fee

                # create accounts if needed
                if txn.from_address not in state.accounts:
                    state.accounts[txn.from_address] = AccountState(balance=0)
                if txn.to_address not in state.accounts:
                    state.accounts[txn.to_address] = AccountState(balance=0)  

                # deduct fee, and amount from sender
                state.accounts[txn.from_address].balance -= txn.amount
                state.accounts[txn.from_address].balance -= txn.fee
                if state.accounts[txn.from_address].balance < 0:
                    print(f"❌ {txn.from_address} has insufficient balance")
                    return False, state

                # credit amount to receiver
                state.accounts[txn.to_address].balance += txn.amount
                

        return True, state
