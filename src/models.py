from datetime import datetime
from typing import Tuple, Dict, Any, List, Optional
import hashlib
from pydantic import BaseModel
from decimal import Decimal
import simplejson as json

# a 'raw' block-line
class ParsedBlock(BaseModel):
    block_number: int
    address: str
    # original block line from file
    raw_line: str  

# a 'raw' transaction-line
class ParsedTransaction(BaseModel):
    from_address: str
    to_address: str
    amount: Decimal
    fee: Decimal
    # original txn line from file
    raw_line: str 

class Transaction:
    def __init__(self, parsed_transaction:ParsedTransaction):
        self.from_address = parsed_transaction.from_address
        self.to_address = parsed_transaction.to_address
        self.amount = parsed_transaction.amount
        self.fee = parsed_transaction.fee
        self.raw_line = parsed_transaction.raw_line


# actually a Block (and its transactions)
class Block:
    block_number: int
    block_address: str
    # each Block stores prev block's hash and current hash
    prev_block_hash: str
    block_hash: str
    block_transactions : List[Transaction] = []

    def __init__(self, block: ParsedBlock, transactions: List[ParsedTransaction]):
        self.block_address = block.address
        self.block_number = block.block_number
        self.block_transactions = transactions

    def is_valid(self) -> bool:
        """Check if block transaction is valid
        @return: A boolean indicating whether the block transaction is valid
        """
        return True


    # format : Note this is used to compute hash, so it wont include the current hash (i.e itself)
    # it will only include the prev hash.
    def _format_as_str(self) -> str:
        out_str = f"{self.prev_block_hash}\n"
        for txn in self.block_transactions:
            txn_str=f"transfer | {txn.from_address} | {txn.to_address} | {txn.amount} | {txn.fee}\n"
            out_str += txn_str
        temp_str = f"coinbase | {self.block_address}\n"
        out_str += temp_str
        return out_str

    # includes current hash.    
    def format_full(self) -> str:
        out_str = self._format_as_str()
        out_str += f"{self.block_hash}\n"
        return out_str
    

    def compute_hash(self) -> str:
        in_str = self._format_as_str()
        self.block_hash =  hashlib.sha256(in_str.encode(encoding="utf-8")).hexdigest()
        return self.block_hash
    

    def export(self, folder: str) -> None:
        """Export block transaction to file
        @param folder: The folder to export to
        """
        with open(f"{folder}/block{self.block_number}.txt", "w") as block_file:
            out_str = self.format_full()
            block_file.write(f"{out_str}\n")

    def __repr__(self) -> str:
        return self.format_full()


# for now, we only store balance for an account state
class AccountState:
    balance: Decimal

    def __init__(self, balance: Decimal):
        self.balance = balance



# state of the block chain
class State:
    accounts = {}

    def __init__(self, accounts: Dict[str, AccountState]):
        self.accounts = accounts

    def is_valid(self) -> bool:
        """Check if state is valid
        @return: A boolean indicating whether the state is valid
        """
        for address, state in self.accounts.items():
            if state.balance < 0:
                return False
        return True

    def export(self, folder: str) -> None:
        """Export state to file
        @param folder: The folder to export to
        """
        export_dict = {}
        for address, account_state in self.accounts.items():
            export_dict[address] = {"balance": account_state.balance}
        with open(f"{folder}/state.json", "w") as state_file:
            json.dump(export_dict, state_file)

    def json(self) -> None:
        """Export state to json
        """
        export_dict = {}
        for address, account_state in self.accounts.items():
            export_dict[address] = {"balance": account_state.balance}
        return json.dumps(export_dict)


# a block chain is a collection of blocks
class BlockChain:
    state: State
    blocks: List[Block]

    def __init__(self, state:State, blocks:List[Block] ) -> None:
        self.blocks = blocks
        self.state = state

    def verify(self):
        return True