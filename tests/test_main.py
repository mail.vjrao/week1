import pytest
from decimal import Decimal
import json
import logging
from input_parser import InputParser
from miner import Miner
from models import   State, AccountState, BlockChain

def test_valid_transaction1():
    input_lines = [
            "coinbase | 0x0a",
            "coinbase | 0x0b",
            "transfer | 0x0a | 0x10 | 2 | 0.1",
            "transfer | 0x0a | 0x11 | 1 | 0.4",
            "transfer | 0x0b | 0x12 | 3 | 0.2",
            "coinbase | 0x0c",
            "transfer | 0x0a | 0x10 | 1 | 0.1",
            "transfer | 0x0c | 0x12 | 4 | 0.2",
            "coinbase | 0x0b"
    ]
    expected_state = State(accounts={})
    expected_state.accounts["0x0a"] = AccountState(balance=Decimal('0.4'))
    expected_state.accounts["0x0b"] = AccountState(balance=Decimal('7.1'))    
    expected_state.accounts["0x11"] = AccountState(balance=Decimal('1.0'))    
    expected_state.accounts["0x12"] = AccountState(balance=Decimal('7.0'))    
    expected_state.accounts["0x10"] = AccountState(balance=Decimal('3.0'))    
    expected_state.accounts["0x0c"] = AccountState(balance=Decimal('1.5')) 
    
    
    ok, block_txns = InputParser.process_input(input_lines, False, False)
    result, mined_state = Miner.mine_blocks(block_txns)
  
    for acct_id, acct_state in mined_state.accounts.items():
        assert mined_state.accounts[acct_id].balance == expected_state.accounts[acct_id].balance 


def test_valid_transaction2():
    input_lines = [
            "coinbase | 0x0c",
            "transfer | 0x0c | 0x10 | 3 | 0.1",
            "transfer | 0x0c | 0x0a | 1 | 0.3",
            "coinbase | 0x0b",
            "transfer | 0x0b | 0x0c | 1 | 0.1",
            "transfer | 0x10 | 0x12 | 2 | 0.5",
            "transfer | 0x0b | 0x12 | 1 | 0.2",
            "coinbase | 0x0b"
    ]

    expected_state = State(accounts={})
    expected_state.accounts["0x0c"] = AccountState(balance=Decimal('1.6'))
    expected_state.accounts["0x0a"] = AccountState(balance=Decimal('1.0'))    
    expected_state.accounts["0x10"] = AccountState(balance=Decimal('0.5'))    
    expected_state.accounts["0x0b"] = AccountState(balance=Decimal('8.9'))    
    expected_state.accounts["0x12"] = AccountState(balance=Decimal('3.0'))    

    ok, block_txns = InputParser.process_input(input_lines, False, False)
    result, mined_state = Miner.mine_blocks(block_txns)
  
    for acct_id, acct_state in mined_state.accounts.items():
        assert mined_state.accounts[acct_id].balance == expected_state.accounts[acct_id].balance 
  

def test_invalid_transaction1():
    input_lines = [
            "coinbase | 0x01",
            "transfer | 0x01 | 0x02 | 5 | 0.1 ",
            "coinbase | 0x03",
    ]
    ok, block_txns = InputParser.process_input(input_lines, False, False)
    result, state = Miner.mine_blocks(block_txns)
    assert result == False
    assert state.is_valid() == False

def test_invalid_transaction2():
    input_lines = [
            "coinbase | 0x0c",
            "transfer | 0x0c | 0x10 | 4 | 0.1",
            "transfer | 0x0a | 0x0d | 2 | 0.1",
            "coinbase | 0x0b",
            "transfer | 0x0b | 0x0c | 1 | 0.1",
            "transfer | 0x0c | 0x20 | 2 | 0.2",
            "transfer | 0x20 | 0x12 | 4 | 0.2",
            "transfer | 0x12 | 0x0d | 4 | 0.2",
            "transfer | 0x0c | 0x0a | 2 | 0.2",
            "coinbase | 0x0b",
    ]
    ok, block_txns = InputParser.process_input(input_lines, False, False)
    result, state = Miner.mine_blocks(block_txns)
    assert result == False
    assert state.is_valid() == False


def test_exports():
    input_lines = [
            "coinbase | 0x0a",
            "coinbase | 0x0b",
            "transfer | 0x0a | 0x10 | 2 | 0.1",
            "transfer | 0x0a | 0x11 | 1 | 0.4",
            "transfer | 0x0b | 0x12 | 3 | 0.2",
            "coinbase | 0x0c",
            "transfer | 0x0a | 0x10 | 1 | 0.1",
            "transfer | 0x0c | 0x12 | 4 | 0.2",
            "coinbase | 0x0b"
    ]
    ok, block_txns = InputParser.process_input(input_lines, False, False)
    result, state = Miner.mine_blocks(block_txns)

    state.export("data")

    for block in block_txns:
        block.export("data")


def test_build_chain():
    input_lines = [
            "coinbase | 0x0c",
            "transfer | 0x0c | 0x10 | 3 | 0.1",
            "transfer | 0x0c | 0x0a | 1 | 0.3",
            "coinbase | 0x0b",
            "transfer | 0x0b | 0x0c | 1 | 0.1",
            "transfer | 0x10 | 0x12 | 2 | 0.5",
            "transfer | 0x0b | 0x12 | 1 | 0.2",
            "coinbase | 0x0b"
    ]


    ok, blocks = InputParser.process_input(input_lines, False, False)
    result, mined_state = Miner.mine_blocks(blocks)
    chain = BlockChain(mined_state, blocks)
    assert chain.verify() == True
  
  